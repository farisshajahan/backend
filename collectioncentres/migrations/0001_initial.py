# Generated by Django 2.2.6 on 2019-10-26 20:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CollectionCentre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('latitude', models.CharField(help_text='Latitude', max_length=20)),
                ('longitude', models.CharField(help_text='Longitude', max_length=20)),
                ('name', models.CharField(help_text='Collection Centre name', max_length=200)),
                ('address', models.CharField(help_text='Collection Centre Address', max_length=500)),
                ('manager', models.CharField(help_text='Collection Centre Manager', max_length=200)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('modified_by', models.ForeignKey(help_text='Collection Centre creator', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ItemCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='ItemName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Item Name', max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Unit of measurement', max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='InventoryLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(help_text='Quantity in stock')),
                ('donor_name', models.CharField(max_length=200)),
                ('donor_address', models.CharField(max_length=500)),
                ('added_date', models.DateTimeField(auto_now=True)),
                ('added_by', models.ForeignKey(help_text='Collection Centre creator', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='collectioncentres.ItemCategory')),
                ('collection_centre', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='collectioncentres.CollectionCentre')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='collectioncentres.ItemName')),
                ('unit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='collectioncentres.Unit')),
            ],
        ),
        migrations.CreateModel(
            name='CollectionCentreInventorySummary',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(default=0, help_text='Quantity in stock')),
                ('required_quantity', models.IntegerField(default=0, help_text='Quantity required')),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='collectioncentres.ItemCategory')),
                ('collection_centre', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='collectioncentres.CollectionCentre')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='collectioncentres.ItemName')),
                ('unit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='collectioncentres.Unit')),
            ],
        ),
    ]
