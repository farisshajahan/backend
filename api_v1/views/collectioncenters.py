from rest_framework import generics
from collectioncentres.models import CollectionCentre
from collectioncentres.serializer import CollectionCentreSerializer


class CollectionCentreList(generics.ListCreateAPIView):
    queryset = CollectionCentre.objects.all()
    serializer_class = CollectionCentreSerializer